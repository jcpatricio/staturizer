import json
from pprint import pprint
from urllib import request
from pokemon.models import Pokemon, PokemonAttack, PokemonType

with open('pokemon/poke.json') as data_file:
    data = json.load(data_file)

multi_evolution_exception = ["Poliwhirl", "Eevee", "Gloom", "Tyrogue"]
# pprint(data[2])
for i, pokemon in enumerate(data):
    # pprint(pokemon)
    if len(pokemon) == 0:
        continue
    (p, _) = Pokemon.objects.get_or_create(name=pokemon["Name"], number=pokemon["Number"])
    p.flavor = pokemon["About"]
    p.base_attack = int(pokemon["Base Attack"].split()[0])
    p.base_defense = int(pokemon["Base Defense"].split()[0])
    p.base_stamina = int(pokemon["Base Stamina"].split()[0])
    p.as_buddy_distance = int(pokemon["Buddy Distance"][0])
    p.generation = 1 if pokemon["Generation"] == "Generation I" else 2
    p.maxCP = int(pokemon["MaxCP"]) if "MaxCP" in pokemon else 0
    p.maxHP = int(pokemon["MaxHP"]) if "MaxHP" in pokemon else 0
    p.candy_evolve_cost = pokemon["Next Evolution Requirements"]["Amount"] if "Next Evolution Requirements" in pokemon else None
    p.type.clear()
    for t in pokemon["Types"]:
        pt = PokemonType.objects.get(name=t)
        p.type.add(pt)
    p.evolves_to.clear()
    if "Next evolution(s)" in pokemon:
        if p.name in multi_evolution_exception:
            for evolution_data in pokemon["Next evolution(s)"]:
                (evolved_pokemon, _) = Pokemon.objects.get_or_create(name=evolution_data["Name"],
                                                                     number=int(evolution_data["Number"]))
                p.evolves_to.add(evolved_pokemon)
        else:
            evolution_data = pokemon["Next evolution(s)"][0]
            (evolved_pokemon, _) = Pokemon.objects.get_or_create(name=evolution_data["Name"],
                                                                 number=int(evolution_data["Number"]))
            p.evolves_to.add(evolved_pokemon)
    p.resistant.clear()
    for t in pokemon["Resistant"]:
        pt = PokemonType.objects.get(name=t)
        p.resistant.add(pt)
    p.weak_to.clear()
    for t in pokemon["Weaknesses"]:
        pt = PokemonType.objects.get(name=t)
        p.weak_to.add(pt)
    p.moveset.clear()
    for special_attack in pokemon["Special Attack(s)"]:
        ptype = PokemonType.objects.get(name=special_attack["Type"])
        (pattack, _) = PokemonAttack.objects.get_or_create(name=special_attack["Name"],
                                                           attack_type=ptype,
                                                           damage=int(special_attack["Damage"]),
                                                           special=True)
        p.moveset.add(pattack)
    for attack in pokemon["Fast Attack(s)"]:
        ptype = PokemonType.objects.get(name=attack["Type"])
        (pattack, _) = PokemonAttack.objects.get_or_create(name=attack["Name"],
                                                           attack_type=ptype,
                                                           damage=int(attack["Damage"]))
        p.moveset.add(pattack)

    if False:
        request.urlretrieve("https://fevgames.net/wp-content/uploads/pokemon/%s.png" % pokemon["Number"],
                            "static/images/pokemon/%s.png" % p.name)
    print("%s/%s" % (i, len(data)), end="\r")
    p.save()
print("done!")

"""
pt = PokemonType.objects.all()

for t in pt:
    urlretrieve("https://fevgames.net/wp-content/themes/urbannews-child/images/types/%s.png" % t.name.lower(), "static/images/types/%s.png" % t.name)
"""