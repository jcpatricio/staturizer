from django import forms

from pokemon.models import PokemonInstance


class PokemonInstanceForm(forms.ModelForm):
    poke_id = forms.IntegerField(disabled=True)
    poke_name = forms.CharField(disabled=True)

    class Meta:
        model = PokemonInstance
        fields = ("percentage", "poke_id", "poke_name")

    def __init__(self, *args, **kwargs):
        super(forms.ModelForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            poke_instance = PokemonInstance.objects.get(pk=self.instance.pk)  #grab instance.pk here)
            self.fields.update({
                'poke_id': forms.IntegerField(disabled=True, initial=poke_instance.pokemon.number),
                'poke_name': forms.CharField(disabled=True, initial=poke_instance.pokemon.name),
            })
