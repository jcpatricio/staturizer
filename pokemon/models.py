from django.contrib.auth.models import User
from django.db import models


class PokemonType(models.Model):
    name = models.CharField(max_length=25)
    weak_against = models.ManyToManyField("self", blank=True, related_name='impervious_to', symmetrical=False)
    strong_against = models.ManyToManyField("self", blank=True, related_name='susceptible_to', symmetrical=False)

    def __str__(self):
        return self.name


class PokemonAttack(models.Model):
    name = models.CharField(max_length=70)
    attack_type = models.ForeignKey(PokemonType)
    damage = models.SmallIntegerField(default=0)
    special = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Pokemon(models.Model):
    KM1 = 1
    KM3 = 3
    KM5 = 5
    BUDDIES_DISTANCE = (
        (KM1, '1 Km'),
        (KM3, '3 Km'),
        (KM5, '5 Km'),
    )

    EGG2 = 2
    EGG5 = 5
    EGG10 = 10
    EGGS_DISTANCE = (
        (EGG2, '2 Km'),
        (EGG5, '5 Km'),
        (EGG10, '10 Km'),
    )

    GEN1 = 1
    GEN2 = 2
    GEN3 = 3
    EXISTING_GENERATIONS = (
        (GEN1, "Gen. I"),
        (GEN2, "Gen. II"),
        (GEN3, "Gen. III"),
    )

    number = models.SmallIntegerField()
    name = models.CharField(max_length=200)
    type = models.ManyToManyField(PokemonType, blank=True)
    evolves_to = models.ManyToManyField("self", blank=True, related_name='evolves_from', symmetrical=False)
    as_buddy_distance = models.SmallIntegerField(choices=BUDDIES_DISTANCE, blank=True, null=True)
    candy_evolve_cost = models.SmallIntegerField(blank=True, null=True)
    flavor = models.CharField(max_length=300, blank=True, null=True)
    base_attack = models.SmallIntegerField(default=0)
    base_defense = models.SmallIntegerField(default=0)
    base_stamina = models.SmallIntegerField(default=0)
    generation = models.SmallIntegerField(choices=EXISTING_GENERATIONS, blank=True, null=True)
    maxCP = models.SmallIntegerField(default=0)
    maxHP = models.SmallIntegerField(default=0)
    moveset = models.ManyToManyField(PokemonAttack, blank=True, related_name='learned_by')
    resistant = models.ManyToManyField(PokemonType, blank=True, related_name="pokemon_resistant_to")
    weak_to = models.ManyToManyField(PokemonType, blank=True, related_name="pokemon_weak_to")
    hatches_from = models.SmallIntegerField(choices=EGGS_DISTANCE, blank=True, null=True)

    def __unicode__(self):
        return self.name


class PokemonInstance(models.Model):
    pokemon = models.ForeignKey(Pokemon)
    percentage = models.FloatField()
    user = models.ForeignKey(User)

    def __str__(self):
        return self.pokemon.name
