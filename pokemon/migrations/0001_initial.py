# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-17 11:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pokemon',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('type', models.CharField(choices=[('GR', 'Grass'), ('FR', 'Fire'), ('WA', 'Water'), ('EL', 'Electricity')], max_length=2)),
            ],
        ),
        migrations.CreateModel(
            name='PokemonInstance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('percentage', models.FloatField()),
                ('pokemon', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pokemon.Pokemon')),
            ],
        ),
    ]
