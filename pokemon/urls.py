from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^pokedex_default/$', views.pokedex_default, name='pokedex_default'),
    url(r'^pokedex_egg/$', views.pokedex_egg, name='pokedex_egg'),
    url(r'^pokedex_buddy/$', views.pokedex_buddy, name='pokedex_buddy'),
    url(r'^mylist/$', views.my_pokemon, name='mylist'),
    url(r'^comparepokemon/(?P<other_username>\w+)$', views.compare_pokemon, name='comparepokemon'),
    url(r'^(?P<pokemon_id>[0-9]+)/$', views.detail, name='detail'),
]