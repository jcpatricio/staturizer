from django.contrib import admin
from .models import Pokemon, PokemonInstance, PokemonType, PokemonAttack

# Register your models here.
admin.site.register(Pokemon)
admin.site.register(PokemonInstance)
admin.site.register(PokemonType)
admin.site.register(PokemonAttack)

