from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import modelformset_factory, BaseInlineFormSet, inlineformset_factory
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

from pokemon.forms import PokemonInstanceForm
from pokemon.models import Pokemon, PokemonInstance, PokemonType


def index(request):
    types = PokemonType.objects.all()
    return render(request, 'pokedex.html', dict(types=types))


def pokedex_default(request):
    pokemons = Pokemon.objects.all().order_by("number")
    return render(request, 'default_filter.html', dict(pokemons=pokemons))


def pokedex_egg(request):
    pokemon2 = Pokemon.objects.filter(hatches_from=Pokemon.EGG2).order_by("number")
    pokemon5 = Pokemon.objects.filter(hatches_from=Pokemon.EGG5).order_by("number")
    pokemon10 = Pokemon.objects.filter(hatches_from=Pokemon.EGG10).order_by("number")
    return render(request, 'egg_filter.html', dict(pokemon2=pokemon2, pokemon5=pokemon5, pokemon10=pokemon10))


def pokedex_buddy(request):
    pokemon1 = Pokemon.objects.filter(as_buddy_distance=Pokemon.KM1).order_by("number")
    pokemon3 = Pokemon.objects.filter(as_buddy_distance=Pokemon.KM3).order_by("number")
    pokemon5 = Pokemon.objects.filter(as_buddy_distance=Pokemon.KM5).order_by("number")
    return render(request, 'buddy_filter.html', dict(pokemon1=pokemon1, pokemon3=pokemon3, pokemon5=pokemon5))


def my_pokemon(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')
    PokemonInstanceFormSet = modelformset_factory(model=PokemonInstance, form=PokemonInstanceForm, extra=0)
    if request.method == 'POST':
        formset = PokemonInstanceFormSet(request.POST)
        if formset.is_valid():
            formset.save()
            # do something.
    else:
        formset = PokemonInstanceFormSet(queryset=PokemonInstance.objects.filter(user=request.user).order_by("pokemon__number"))
    stats = dict(
        perfect=PokemonInstance.objects.filter(user=request.user, percentage=100).count(),
        ninetyplus=PokemonInstance.objects.filter(user=request.user, percentage__gte=90).count(),
        eightyplus=PokemonInstance.objects.filter(user=request.user, percentage__gte=80).count(),
        count=PokemonInstance.objects.filter(user=request.user).exclude(percentage=0).count(),
    )
    return render(request, 'my_list.html', {'formset': formset, "stats": stats})


def compare_pokemon(request, other_username):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')
    try:
        other_user = User.objects.get(username=other_username)
    except User.DoesNotExist:
        return render(request, 'compare_list.html', {})

    user1PokeInstances = PokemonInstance.objects.filter(user=request.user).order_by("pokemon__number")
    user2PokeInstances = PokemonInstance.objects.filter(user=other_user).order_by("pokemon__number")

    plist = zip(user1PokeInstances, user2PokeInstances)
    return render(request, 'compare_list.html', {"plist": plist})

def detail(request, pokemon_id):
    try:
        pokemon = Pokemon.objects.get(id=pokemon_id)
    except Pokemon.DoesNotExist:
        pokemon = None
    context = {
        'pokemon': pokemon,
    }
    return render(request, 'pokemon_detail.html', context)


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            pokemons = Pokemon.objects.all()
            for pokemon in pokemons:
                PokemonInstance.objects.create(user=user, pokemon=pokemon, percentage=0)
            login(request, user)
            return redirect('index')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})