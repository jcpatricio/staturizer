from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^events/$', views.index, name='events'),
    url(r'^events/(?P<event_id>[0-9]+)/$', views.event_detail, name='event_detail'),
    url(r'^ivcalc/$', views.iv_calc, name='iv_calc'),
]