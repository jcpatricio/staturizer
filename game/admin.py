# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from game.models import GameEvents

admin.site.register(GameEvents)
