# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from pokemon.models import Pokemon


class GameEvents(models.Model):
    name = models.CharField(max_length=150)
    date_start = models.DateTimeField()
    date_end = models.DateTimeField()
    target_pokemon = models.ManyToManyField(Pokemon)
    experience = models.SmallIntegerField(null=True, blank=True)
    star_dust = models.SmallIntegerField(null=True, blank=True)
    lure_modules = models.SmallIntegerField(null=True, blank=True)
    candy = models.SmallIntegerField(null=True, blank=True)
    other_bonus = models.CharField(max_length=500, null=True, blank=True)

    def __unicode__(self):
        return self.name