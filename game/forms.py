from django import forms

DUST_POWER_UP_CHOICES = (
    (200, 200),
    (400, 400),
    (600, 600),
    (800, 800),
    (1000, 1000),
    (1300, 1300),
    (1600, 1600),
    (1900, 1900),
    (2200, 2200),
    (2500, 2500),
    (3000, 3000),
    (3500, 3500),
    (4000, 4000),
    (4500, 4500),
    (5000, 5000),
    (6000, 6000),
    (7000, 7000),
    (8000, 8000),
    (9000, 9000),
    (10000, 10000),
)

IV_RANGE = (
    (0, "---"),
    (1, "Not out of the norm."),
    (2, "Noticeably trending to the positive"),
    (3, "I'm certainly impressed by its stats"),
    (4, "Its stats exceed my calculations!")
)


class IVCalcForm(forms.Form):
    pokemon_name = forms.CharField(label='name', max_length=100)
    cp = forms.IntegerField(label='CP')
    hp = forms.IntegerField(label='HP')
    dust = forms.ChoiceField(
        required=True,
        choices=DUST_POWER_UP_CHOICES,
    )
    powered = forms.TypedChoiceField(coerce=lambda x: x == 'True',
                                     choices=((False, 'No'), (True, 'Yes')))
    attack = forms.BooleanField(required=False)
    defense = forms.BooleanField(required=False)
    stamina = forms.BooleanField(required=False)
    best = forms.ChoiceField(
        required=False,
        choices=IV_RANGE,
    )

