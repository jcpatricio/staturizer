# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import math
from django.shortcuts import render

from game.forms import IVCalcForm
from game.models import GameEvents
from pokemon.models import Pokemon

dust_to_level = {
    200: [1, 2],
    400: [3, 4],
    600: [5, 6],
    800: [7, 8],
    1000: [9, 10],
    1300: [11, 12],
    1600: [13, 14],
    1900: [15, 16],
    2200: [17, 18],
    2500: [19, 20],
    3000: [21, 22],
    3500: [23, 24],
    4000: [25, 26],
    4500: [27, 28],
    5000: [29, 30],
    6000: [31, 32],
    7000: [33, 34],
    8000: [35, 36],
    9000: [37, 38],
    10000: [39, 40],
}

cpscalar = {
	1: 0.0940000,
	1.5: 0.1351374,
	2: 0.1663979,
	2.5: 0.1926509,
	3: 0.2157325,
	3.5: 0.2365727,
	4: 0.2557201,
	4.5: 0.2735304,
	5: 0.2902499,
	5.5: 0.3060574,
	6: 0.3210876,
	6.5: 0.3354450,
	7: 0.3492127,
	7.5: 0.3624578,
	8: 0.3752356,
	8.5: 0.3875924,
	9: 0.3995673,
	9.5: 0.4111936,
	10: 0.4225000,
	10.5: 0.4335117,
	11: 0.4431076,
	11.5: 0.4530600,
	12: 0.4627984,
	12.5: 0.4723361,
	13: 0.4816850,
	13.5: 0.4908558,
	14: 0.4998584,
	14.5: 0.5087018,
	15: 0.5173940,
	15.5: 0.5259425,
	16: 0.5343543,
	16.5: 0.5426358,
	17: 0.5507927,
	17.5: 0.5588306,
	18: 0.5667545,
	18.5: 0.5745692,
	19: 0.5822789,
	19.5: 0.5898879,
	20: 0.5974000,
	20.5: 0.6048188,
	21: 0.6121573,
	21.5: 0.6194041,
	22: 0.6265671,
	22.5: 0.6336492,
	23: 0.6406530,
	23.5: 0.6475810,
	24: 0.6544356,
	24.5: 0.6612193,
	25: 0.6679340,
	25.5: 0.6745819,
	26: 0.6811649,
	26.5: 0.6876849,
	27: 0.6941437,
	27.5: 0.7005429,
	28: 0.7068842,
	28.5: 0.7131691,
	29: 0.7193991,
	29.5: 0.7255756,
	30: 0.7317000,
	30.5: 0.7377735,
	31: 0.7377695,
	31.5: 0.7407856,
	32: 0.7437894,
	32.5: 0.7467812,
	33: 0.7497610,
	33.5: 0.7527291,
	34: 0.7556855,
	34.5: 0.7586304,
	35: 0.7615638,
	35.5: 0.7644861,
	36: 0.7673972,
	36.5: 0.7702973,
	37: 0.7731865,
	37.5: 0.7760650,
	38: 0.7789328,
	38.5: 0.7817901,
	39: 0.7846370,
	39.5: 0.7874736,
	40: 0.7903000,
	40.5: 0.7931164
}

iv_filters = {
    1: range(1, 8),
    2: range(8, 13),
    3: range(13, 15),
    4: [15]
}


def index(request):
    events = GameEvents.objects.all().order_by("-date_start")
    return render(request, 'events.html', dict(events=events))


def event_detail(request, event_id):
    event = GameEvents.objects.get(id=event_id)
    return render(request, 'event_detail.html', dict(event=event))


def iv_calc(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = IVCalcForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            pname = form.cleaned_data["pokemon_name"]
            try:
                p = Pokemon.objects.get(name=pname)
            except Pokemon.DoesNotExist:
                return render(request, 'ivcalc.html', {'form': form, "no_pokemon": True})
            print("I have pokemon")
            dust = form.cleaned_data["dust"]
            possible_levels = dust_to_level[int(dust)]
            powered = form.cleaned_data["powered"]
            cp = int(form.cleaned_data["cp"])
            hp = int(form.cleaned_data["hp"])
            attack = form.cleaned_data["attack"]
            defense = form.cleaned_data["defense"]
            stamina = form.cleaned_data["stamina"]
            best = form.cleaned_data["best"]
            print(best)
            possible_aiv_values = iv_filters[int(best)] if attack and best else range(16)[1:]
            possible_div_values = iv_filters[int(best)] if defense and best else range(16)[1:]
            possible_siv_values = iv_filters[int(best)] if stamina and best else range(16)[1:]

            if powered:
                possible_levels += [x+0.5 for x in possible_levels]
            possible_ivs = list()
            percentage_stats = dict()
            for level in possible_levels:
                cpsca = cpscalar[level]
                # HP = (BaseStam + StamIV) * Lvl(CPScalar)
                # for a given level it works out possible stamina values.
                # Each level has a CP multiplier ( in the game data sheet).
                # IV stamina can be anything from 0 to 15.
                # It adds this value to the base stamina of the pokemon and multiplies it by the CP value.
                # It then takes the floor of this value (so taking just the whole number part so 59.9 would be 59).
                # If this matches the HP you were given then the stamina is a valid possibility.
                stamIV_possibilities = list()
                for siv in possible_siv_values:
                    possible_hp = math.floor((p.base_stamina + siv) * cpsca)
                    if possible_hp == hp:
                        stamIV_possibilities.append(siv)
                print("Possible stamina iv values are: %s" % stamIV_possibilities)
                # CP = (BaseAtk + AtkIV) *(BaseDef + DefIV) ^ 0.5 * (BaseStam + StamIV) ^ 0.5 * Lvl(CPScalar) ^ 2 / 10
                for aiv in possible_aiv_values:
                    for div in possible_div_values:
                        for siv in stamIV_possibilities:
                            possible_cp = (p.base_attack + aiv) * \
                                          math.sqrt(p.base_defense + div) * \
                                          math.sqrt(p.base_stamina + siv) * (cpsca ** 2) / 10
                            possible_cp = math.floor(possible_cp)
                            if possible_cp == cp:
                                percentage = ((aiv + div + siv) * 100) / 45
                                possible_ivs.append([aiv, div, siv, percentage])
                percentages = [x[-1] for x in possible_ivs] if len(possible_ivs) > 0 else [0]
                percentage_stats = dict(max=max(percentages), min=min(percentages), avg=sum(percentages)/len(percentages))
            return render(request, 'ivcalc.html', {'form': form, 'pname': pname, 'possible_ivs': possible_ivs,
                                                   'percentages': percentage_stats})
    else:
        form = IVCalcForm()

    return render(request, 'ivcalc.html', {'form': form, "noform": True})