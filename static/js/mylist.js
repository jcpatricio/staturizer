/**
 * Created by Valdieme on 26/05/2017.
 */
window.onload = function() {
    var row_percentages = $(".row_percentage");
    $(".row_percentage > input").focus(function() {
       $(this).select();
    });
    row_percentages.each(function () {
        var row_percentage = this;
        var pvalue = parseInt($(row_percentage).find("input").val());
        var all_row = $(row_percentage).parent("tr");
        if (pvalue >= 90) {
            all_row.addClass("table-success");
        } else if (pvalue >= 80){
            all_row.addClass("table-info");
        } else if (pvalue === 0){
            all_row.addClass("table-danger");
        } else {
            all_row.addClass("table-warning");
        }
        $(this).find("input").on('keyup', function() {
            var pvalue = parseInt($(this).val());
            var all_row = $(this).parent("tr");
            if (pvalue >= 90) {
                all_row.addClass("table-success");
            } else if (pvalue >= 80){
                all_row.addClass("table-info");
            } else {
                all_row.addClass("table-warning");
            }
        });

    });

    $("#compare").click(function(){
        var other_user = $("#other_user").val();
        window.location = "/pokemon/comparepokemon/" + other_user;
    })
};