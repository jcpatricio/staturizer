/**
 * Created by Valdieme on 26/05/2017.
 */
window.onload = function() {
  // alert('hi!');
    var u1_dex_entries = 0;
    var u2_dex_entries = 0;
    var u1_sum = 0;
    var u2_sum = 0;
    $(".prow").each(function () {
        var user1_val = parseInt($(this).find(".u1p").html());
        var user2_val = parseInt($(this).find(".u2p").html());
        u1_dex_entries += ((user1_val > 0) ? 1 : 0);
        u2_dex_entries += ((user2_val > 0) ? 1 : 0);
        u1_sum += user1_val;
        u2_sum += user2_val;
        if (user1_val > user2_val){
            $(this).find(".u1p").addClass("table-success");
            $(this).find(".u2p").addClass("table-danger");
        } else if (user2_val > user1_val) {
            $(this).find(".u2p").addClass("table-success");
            $(this).find(".u1p").addClass("table-danger");
        } else {
            $(this).find(".u2p").addClass("table-info");
            $(this).find(".u1p").addClass("table-info");
        }
    });
    $("#u1avg").html((u1_sum/u1_dex_entries).toFixed(2));
    $("#u2avg").html((u2_sum/u2_dex_entries).toFixed(2));
    $("#u1dex").html(u1_dex_entries);
    $("#u2dex").html(u2_dex_entries);
};