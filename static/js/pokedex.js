/**
 * Created by Valdieme on 26/05/2017.
 */

function change_content(url){
    $.ajax({
      url: url,
      success: function(data) {
        $('#filter_type').html(data);
      }
    });
}

window.onload = function() {

    $('[data-toggle="tooltip"]').tooltip();

    $(".poke-type").click(function() {
        var type = $(this).attr("id").toLowerCase();

        if ($(this).find("img").hasClass("desaturate")) {
            // Show all with this class
            $( ".dex-mon" ).each(function() {
                if ($(this).hasClass(type)){
                    // console.log("Found pokemon: " + $(this).find("img").attr("alt"));
                    $(this).show();
                }
            });
        } else {
            // Hide all with this
            $( ".dex-mon" ).each(function() {
                if ($(this).hasClass(type)){
                    // console.log("Found pokemon: " + $(this).find("img").attr("alt"));
                    $(this).hide();
                }
            });
        }
        $(this).find("img").toggleClass("desaturate");
    });

    $(".filter").click(function(e){
        e.preventDefault();
        $(".nav-link").removeClass("active");
        $(this).addClass("active");
        var chosen_filter = $(this).attr("id");
        console.log(chosen_filter);
        switch(chosen_filter) {
            case "egg":
                change_content('/pokemon/pokedex_egg/');
                $("#type_filter_row").hide();
                break;
            case "default":
                change_content('/pokemon/pokedex_default/');
                $("#type_filter_row").show();
                break;
            case "buddy":
                change_content('/pokemon/pokedex_buddy/');
                $("#type_filter_row").hide();
                break;
            default:
                console.log("uh?")
        }

    });

    $.ajax({
      url: '/pokemon/pokedex_default/',
      success: function(data) {
        $('#filter_type').html(data);
      }
    });

};